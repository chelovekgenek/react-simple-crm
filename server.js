const webpack = require('webpack')
const express = require('express')
const cors = require('cors')
const app = express()
const compression = require('compression')
const bodyParser = require('body-parser')
const fetch = require('node-fetch')


const settingsJSON  = require('./resources/settings.json')
const dataJSON      = require('./resources/data.json')

/**
 * Production mode build
 * `set NODE_ENV=production`
 * `webpack -p --config ./webpack.production.config.js`
 * `npm start`
 */

app.use(compression())
app.use(cors())
app.use(bodyParser.json())

var config;
if (process.env.NODE_ENV == 'production') {
  console.log('PRODUCTION MODE')
  app.use('/static', express.static('dist'))
  config = require('./config/production.json')
} else {
  console.log('DEBUG MODE')
  const webpackDevMiddleware = require('webpack-dev-middleware')
  const webpackHotMiddleware = require('webpack-hot-middleware')
  const wpconfig = require('./webpack.config')
  var compiler = webpack(wpconfig)
  app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: wpconfig.output.publicPath }))
  app.use(webpackHotMiddleware(compiler))
  config = require('./config/default.json')
}


app.post('/api/reportconfig/getall', getAll)

app.post('/api/reportconfig/params', getParams)

app.post('/api/reportconfig/add', postReport)

app.post('/api/reportgeneration/generatereport', postGenericReport)

app.get('*', (req, res) => {
  res.sendFile(__dirname + '/index.html')
})

app.listen(config.port, function(error) {
  if (error) {
    console.error(error)
  } else {
    console.info('==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.', config.port, config.port)
  }
})

function getIndexHtml(res) {
  res.sendFile(__dirname + '/index.html')
}
function redirectUnmatched(req, res) {
  res.redirect('/')
}

const headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
}

function getAll(req, res) {
  const options = {
    method: 'POST',
    headers
  }
  fetch('http://localhost:8095/rest/v1/reportconfig/getall', options)
    .then(response => response.json())
    .then(result => res.json(result))
}

function getParams(req, res) {
  const options = {
    method: 'POST',
    headers
  }
  fetch('http://localhost:8095/rest/v1/reportconfig/params', options)
    .then(response => response.json())
    .then(result => res.json(result))
}

function postReport(req, res) {
  const options = {
    method: 'POST',
    headers,
    body: JSON.stringify(req.body)
  }
  fetch('http://localhost:8095/rest/v1/reportconfig/add', options)
    .then(response => res.status(200).send())
}

function postGenericReport(req, res) {
  const options = {
    method: 'POST',
    headers,
    body: JSON.stringify(req.body)
  }
  fetch('http://localhost:8095/rest/v1/reportgeneration/generatereport', options)
    .then(response => response.json())
    .then(result => res.json(result))
}
