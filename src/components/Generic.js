import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { redirect } from '../actions/app'
import { ROUTE_LIST, genericList } from '../constants'

class Generic extends Component {
  render() {
    let data = []
    //data = genericList
    const generic = this.props.generic
    if (Object.keys(generic).length)
      data = generic
    return (
      <div>
        <h2>Generic Usage Reporting Template</h2>
        <div className='row offset-top'>
          <div className='left'>
            <button 
              className='back-btn'
              onClick={() => this.props.redirect(ROUTE_LIST)}
            >
              <img src={require('../style/assets/chevron-left.svg')} className='chevron-icon'></img>
              <span>Back to Reports</span>
            </button>
          </div>
        </div>
        <div className='row'>
          { this.getReportsTable(data) }
        </div>
      </div>
    )
  }
  getReportsTable(data) {
    return (
      <table className='table_reports generic'>
        <thead>
          <tr>
            {data.reportHeader.map((item, i) => (
              <th key={i}>{item}</th>
            ))}
          </tr>
        </thead>
        <tbody>

        { data.reportBody.map((item, i) => (
          <tr key={i}>
            { item.map((subitem, j) => (
              <td key={j}>{ subitem }</td>
            ))}
          </tr>
        ))}
        </tbody>
      </table>
    )
  }
}

function mapStateToProps(state) {
  return {
    generic: state.rootReducer.app.generic
  }
}
function mapDispatchToProps(dispatch) {
  return {
    redirect: bindActionCreators(redirect, dispatch),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Generic)
