import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Pagination from './common/Pagination'

import { redirect, getAll, getGenericUsageReport } from '../actions/app'
import { reportsList, ROUTE_ACTION } from '../constants'

import '../style/components/list.css'

class List extends Component {
  state = {
    currPage: 1,
    totalPages: 0,
    onPage: 5,
    onPageCount: [5, 10, 20, 50, 100]
  }
  componentWillMount() {
    this.props.getAll()
    /*this.setState({
      currPage: 1,
      totalPages: Math.ceil(reportsList.length / this.state.onPage)
    })*/
  }
  componentWillReceiveProps(nextProps) {
    const data = nextProps.app.data
    if (data.length) {
      this.setState({
        currPage: 1,
        totalPages: Math.ceil(data.length / this.state.onPage)
      })
    }
  }
  render() {
    let reports = [], data = this.props.app.data
    if (data.length) {
      const start = (this.state.currPage - 1) * this.state.onPage
      reports = this.props.app.data.slice(start, start + this.state.onPage)
    }
    /*let reports = [], data = reportsList
    if (data.length) {
      const start = (this.state.currPage - 1) * this.state.onPage
      reports = data.slice(start, start + this.state.onPage)
    }*/
    return (
      <div>
        <div className='row'>
          <h2>Usage Reports</h2>
        </div>
        <div className='row'>
          <div className='right'>
            <button onClick={() => this.props.redirect(ROUTE_ACTION)}>New Report</button>
          </div>
        </div>
        <div className='row offset-top'>
          <div className='left'>
            <h3>Reports</h3>
          </div>
          <div className='right list-pagination'>
            <select
              className='list-on-page'
              value={this.state.onPage}
              onChange={this.handleSelectOnPage.bind(this)}
            >
              { this.state.onPageCount.map((item, i) => (
                <option key={i} value={item}>{item}</option>
              ))}
            </select>
            {
              <Pagination
                currPage={this.state.currPage}
                totalPages={this.state.totalPages}
                setPage={page => this.setState({currPage: page})}
              />
            }
          </div>
        </div>
        <div className='row'>
          { this.getReportsTable(reports) }
        </div>
      </div>
    )
  }
  getReportsTable(data) {
    return (
      <table className='table_reports'>
        <thead>
          <tr>
            <th className='col_id'>Id</th>
            <th className='col_name'>Name</th>
            <th className='col_report-type'>Report Type</th>
            <th className='col_report-start-date'>reportStartDate</th>
            <th className='col_report-end-date'>reportEndDate</th>
            <th className='col_report-actions'>Actions</th>
          </tr>
        </thead>
        <tbody>
          { data.map((item, i) => (
            <tr key={i}>
              <td className='col_id'>{item.id}</td>
              <td className='col_name'>
                <a href='#' onClick={() => this.props.getGenericUsageReport(item.id)}>
                  { item.name }
                </a>
              </td>
              <td className='col_report-type'>{ item.reportType }</td>
              <td className='col_report-start-date'>{ new Date(item.reportStartDate).toISOString() }</td>
              <td className='col_report-end-date'>
                { item.reportEndDate ? new Date(item.reportEndDate).toISOString() : 'null' }
              </td>
              <td className='col_report-actions'>
                <a href='#'>Edit</a>
                <a href='#'>Delete</a>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    )
  }
  handleSelectOnPage(e) {
    const value = e.target.value
    this.setState({
      onPage: parseInt(value),
      currPage: 1,
      totalPages: Math.ceil(this.props.app.data.length / value)
      //totalPages: Math.ceil(reportsList.length / value)
    })
  }
  /*getRecurringJobLogTable(data) {
    return (
      <Table
        selectable={false}
      >
        <TableHeader
          adjustForCheckbox={false}
          displaySelectAll={false}
        >
          <tr>
            <th>
              Description
            </th>
            <th>
              User
            </th>
            <th>
              Start time
            </th>
            <th>
              End time
            </th>
            <th>
              Run time
            </th>
          </tr>
        </TableHeader>
        <TableBody
          displayRowCheckbox={false}
          showRowHover={true}
        >
        { data.map((item, i) => (
          <tr key={i}>
            <tr>
              { item.description }
            </tr>
            <tr>
              { item.user }
            </tr>
            <tr>
              { item.startTime }
            </tr>
            <tr>
              { item.endTime }
            </tr>
            <tr>
              { item.runTime }
            </tr>
          </tr>
        ))}
        </TableBody>
      </Table>
    )
  }*/
}

function mapStateToProps(state) {
  return {
    app: state.rootReducer.app
  }
}
function mapDispatchToProps(dispatch) {
  return {
    redirect: bindActionCreators(redirect, dispatch),
    getAll: bindActionCreators(getAll, dispatch),
    getGenericUsageReport: bindActionCreators(getGenericUsageReport, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(List)
