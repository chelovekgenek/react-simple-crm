import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { redirect } from '../actions/app'
import { ROUTE_EDIT } from '../constants'

import '../style/components/action.css'

const ChevronDoubleRight = require('../style/assets/chevron-double-right.svg')

const data = [{
    header: 'Content Usage Report',
    description: 'Analyze site traffic by title usage, view types, content types and access types',
    route: ROUTE_EDIT
  }, {
    header: 'Session Report',
    description: 'Analyze site traffic by session count',
  }
]

class Action extends Component {
  render() {
    return (
      <div>
        <h2>Create New Report</h2>
        <div className='offset-top'>
          <h3>Report Types</h3>
          { data.map((item, i) => (
            <div key={i} className='small-offset-top'>
              { this.getButton(item) }
            </div>
          ))}
        </div>
      </div>
    )
  }
  getButton(item) {
    return (
      <button onClick={() => item.route ? this.props.redirect(item.route) : alert('Not implemented.')}>
        <div className='action-btn'>
          <div className='left'>
            <p className='header'>Create new <h4 className='col'>{item.header}</h4></p>
            <p className='description'>{item.description}</p>
          </div>
          <div className='right icon'>
            <img src={ChevronDoubleRight} />
          </div>
        </div>
      </button>
    )
  }
}

function mapStateToProps(state) {
  return {
  }
}
function mapDispatchToProps(dispatch) {
  return {
    redirect: bindActionCreators(redirect, dispatch),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Action)
