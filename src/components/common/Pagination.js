import React, { Component } from 'react'

import '../../style/pagination.css'


export default class Pagination extends Component {
  render() {
    let prev_pages = [], next_pages = []
    for (let i = 1; i > 0 && i < this.props.currPage; i++)
      prev_pages.push(this.getNumBtn(i))
    for (let i =this.props.currPage + 1; i <= this.props.totalPages; i++)
      next_pages.push(this.getNumBtn(i))
    return(
      <div className='pagination'>
        <button 
          className='chevron-btn'
          disabled={this.props.currPage <= 1}
          onClick={() => this.props.setPage(this.props.currPage - 1)}
        >
          <img src={require('../../style/assets/chevron-left.svg')} className='chevron-icon'></img>
        </button>
        {prev_pages}
        { this.getNumBtn(this.props.currPage, true) }
        {next_pages}
        <button 
          className='chevron-btn'
          disabled={this.props.currPage >= this.props.totalPages}
          onClick={() => this.props.setPage(this.props.currPage + 1)}
        >
          <img src={require('../../style/assets/chevron-right.svg')} className='chevron-icon'></img>
        </button>
      </div>
    )
  }
  getNumBtn(i, current = false) {
    return (
      <button 
        className='text-btn'
        key={i} disabled={current} 
        onClick={() => this.props.setPage(i)}
      >
        {i}
      </button>
    )
  }
}
