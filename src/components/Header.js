import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { redirect } from '../actions/app'
import {
  ROUTE_LIST,
  ROUTE_ACTION,
  ROUTE_EDIT
} from '../constants'

class Header extends Component {
  render() {
    return (
      <div>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
  }
}
function mapDispatchToProps(dispatch) {
  return {
    redirect: bindActionCreators(redirect, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header)
