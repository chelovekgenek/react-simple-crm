import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import DatePicker from 'react-datepicker'
import moment from 'moment'

import { initContentUsageReport, saveContentUsageReport } from '../actions/app'

import '../style/components/edit.css'
import 'react-datepicker/dist/react-datepicker.css'

class Edit extends Component {
  state = {
    reportName: '',
    accountsSelected: 0,
    primaryResultRow: 0,
    secondaryResultRow: '',
    resultGrouping: 0,
    viewType: '',
    contentType: '',
    accessType: '', 
    startDate: moment(),
    endDate: moment(),
    reportNameError: false
  }
  componentWillMount() {
    this.props.initContentUsageReport()
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.app.fetching)
      return false
    else 
      return true
  }
  componentWillReceiveProps(nextProps) {
    if (Object.keys(nextProps.app.settings).length)
      this.setState({
        primaryResultRow: nextProps.app.settings.resultType[0].name,
        resultGrouping:   nextProps.app.settings.resultGroupingType[0].name,
        viewType:         nextProps.app.settings.viewType[0].name,
        contentType:      nextProps.app.settings.contentTypes[0].name,
        accessType:       nextProps.app.settings.accessTypes[0].name
      })
  }
  render() {
    const settings = this.props.app.settings
    return (
      <div>
        <h2>New/Edit Content Usage Report</h2>
          <div className='panel'>
            <h1 className='divider'>General</h1>
            <div className='content general'>
              <div className='row'>
                <span>Report Name</span>
                <input 
                  value={this.state.reportName} 
                  onChange={this.handleReportName.bind(this)}
                />
              </div>
            </div>
        </div>
          <div className='panel'>
            <h1 className='divider'>Result Row Format</h1>
            <div className='content rrf'>
              <div className='row'>
                <div className='text-block'>Primary Result Row</div>
                <select 
                  value={this.state.primaryResultRow}
                  onChange={e => this.setState({primaryResultRow: e.target.value})}
                > 
                  { settings.resultType.map((item, i) => (
                    <option key={i} value={item.name}>{item.value}</option>
                  ))}
                </select>
              </div>
              <div className='row'>
                <div className='text-block'>Secondary Result Row (Optional)</div>
                <select 
                  value={this.state.secondaryResultRow}
                  onChange={e => this.setState({secondaryResultRow: e.target.value})}
                >
                  { <option key={0} value=''></option>}
                  {  settings.resultType.map((item, i) => (
                    <option key={i} value={item.name}>{item.value}</option>
                    ))}
                </select>
              </div>
              <div className='row'>
                <div className='text-block'>Result Grouping</div>
                <select 
                  value={this.state.resultGrouping}
                  onChange={e => this.setState({resultGrouping: e.target.value})}
                >
                  { settings.resultGroupingType.map((item, i) => (
                    <option key={i} value={item.name}>{item.value}</option>
                  ))}
                </select>
              </div>
            </div>
        </div>
        <div className='panel'>
          <h1 className='divider'>Filters</h1>
          <div className='content filters'>
            <div className='row'>
              <div className='text-block'>Accounts</div>
              <input name='All' type='radio' checked={true} />
            </div>
            <div className='row'>
              <div className='text-block'></div>
              <input name='Custom' type='radio' checked={false} />
              <div className='text-block'>Custom</div>
              <input />
              <button>Add</button>
            </div>
            <div className='row'>
              <div className='text-block'></div>
              <input type='radio' checked={false}/>
              <div className='text-block'>Consortium</div>
              <input />
              <button>Add</button>
            </div>
            <div className='row'>
              <div className='text-block'>View Types</div>
              <select 
                value={this.state.viewType}
                onChange={e => this.setState({viewType: e.target.value})}
              >
                { settings.viewType.map((item, i) => (
                  <option key={i} value={item.name}>{item.value}</option>
                ))}
              </select>
            </div>
            <div className='row'>
              <div className='text-block'>Content Types</div>
              <select 
                value={this.state.contentType}
                onChange={e => this.setState({contentType: e.target.value})}
              >
                { settings.contentTypes.map((item, i) => (
                  <option key={i} value={item.name}>{item.value}</option>
                ))}
              </select>
            </div>
            <div className='row'>
              <div className='text-block'>Access Type</div>
              <select 
                value={this.state.accessType}
                onChange={e => this.setState({accessType: e.target.value})}
              >
                { settings.accessTypes.map((item, i) => (
                  <option key={i} value={item.name}>{item.value}</option>
                ))}
              </select>
            </div>
          </div> 
        </div>
        <div className='panel'>
          <h1 className='divider'>Date</h1>
          <div className='content dates'>
            <div className='row'>
              <div className='text-block'>Date Range</div>
              <div className='text-block'>Start</div>
              <DatePicker 
                selected={this.state.startDate}
                onChange={d => this.setState({startDate: d})}
              />
            </div>
            <div className='row'>
              <div className='text-block'></div>
              <div className='text-block'>End</div>
              <DatePicker 
                selected={this.state.endDate}
                onChange={d => this.setState({endDate: d})}
              />
            </div>
          </div>
        </div>
        <div className='row'>
          <button 
            className='confirm-btn'
            onClick={this.save.bind(this)}
          >Save</button> 
          <button
            className='cancel-btn'
          >Cancel</button> 
        </div>
      </div>
    )
  }
  handleReportName(e) {
    const value = e.target.value
    this.setState({reportName: value})
    if (this.state.reportNameError)
      this.setState({reportNameError: ''})
  }
  save() {
    const state = this.state
    if (!state.reportName) 
      return this.setState({reportNameError: 'Field shouldn\'t be empty'})

    let data = {
      reportType:         this.props.app.settings.reportType[0].name,
      clientId:           'IMF',
      name:               this.state.reportName,
      startDate:          this.state.startDate.unix(),
      endDate:            this.state.endDate.unix(),
      primaryUsage:       this.state.primaryResultRow,
      secondaryUsage:     this.state.secondaryResultRow,
      resultGrouping:     this.state.resultGrouping,
      viewTypes: [{
        value: this.state.viewType
      }],
      contentTypes: [{
        value: this.state.contentType
      }],
      accessTypes: [{
        value: this.state.accessType
      }],
      accounts: [{
        accountId: 1,
        accountName: 'Abc Act Name'
      }],
      accountGroups: [{
        accountGroupId: 1,
        accountGroupName: 'Test Account Group'
      }]
    }
    this.props.saveContentUsageReport(data)
  }
}

function mapStateToProps(state) {
  return {
    app: state.rootReducer.app
  }
}
function mapDispatchToProps(dispatch) {
  return {
    initContentUsageReport: bindActionCreators(initContentUsageReport, dispatch),
    saveContentUsageReport: bindActionCreators(saveContentUsageReport, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Edit)