import 'babel-polyfill'
import 'whatwg-fetch'
import React from 'react'
import { Provider } from 'react-redux'
import { render } from 'react-dom'
import { Router, Route, Redirect, IndexRedirect, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import configureStore from './store'

import App      from './containers/App'
import List     from './components/List'
import Action   from './components/Action'
import Edit     from './components/Edit'
import Generic  from './components/Generic'

import {
  ROUTE_INDEX,
  ROUTE_LIST,
  ROUTE_EDIT,
  ROUTE_ACTION,
  ROUTE_GENERIC,
  ROUTE_ANY,
  API_POST_REPORT_SETTINGS_URL
} from './constants'

import './style/app.css'

/*if(module.hot) {
  console.clear()
  module.hot.accept()
}*/

const store = configureStore()
const history = syncHistoryWithStore(browserHistory, store)

render(
  <Provider store={store}>
    <Router history={history}>
      <Route path={ROUTE_INDEX} component={App}>
        <IndexRedirect to={ROUTE_LIST} />
        <Route path={ROUTE_LIST}   component={List} />
        <Route path={ROUTE_ACTION} component={Action} />
        <Route path={ROUTE_EDIT} component={Edit} />
        <Route path={ROUTE_GENERIC} component={Generic} />
        <Redirect from={ROUTE_ANY} to={ROUTE_INDEX} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
)
