
export const ROUTE_INDEX    = '/'
export const ROUTE_ANY      = '*'
export const ROUTE_LIST     = 'list'
export const ROUTE_ACTION   = 'action'
export const ROUTE_EDIT     = 'edit'
export const ROUTE_GENERIC  = 'generic'

export const API_GET_ALL_URL                    = '/api/reportconfig/getall'
export const API_GET_ALL_REQUEST                = 'API_GET_ALL_REQUEST'
export const API_GET_ALL_SUCCESS                = 'API_GET_ALL_SUCCESS'
export const API_GET_ALL_ERROR                  = 'API_GET_ALL_ERROR'

export const API_GET_REPORT_SETTINGS_URL        = 'api/reportconfig/params'
export const API_GET_REPORT_SETTINGS_REQUEST    = 'API_GET_REPORT_SETTINGS_REQUEST'
export const API_GET_REPORT_SETTINGS_SUCCESS    = 'API_GET_REPORT_SETTINGS_SUCCESS'

export const API_POST_REPORT_SETTINGS_URL       = 'api/reportconfig/add'
export const API_POST_REPORT_SETTINGS_REQUEST   = 'API_POST_REPORT_SETTINGS_REQUEST'
export const API_POST_REPORT_SETTINGS_SUCCESS   = 'API_POST_REPORT_SETTINGS_SUCCESS'

export const API_POST_GENERIC_REPORT_URL        = '/api/reportgeneration/generatereport'
export const API_POST_GENERIC_REPORT_REQUEST    = 'API_POST_GENERIC_REPORT_REQUEST'
export const API_POST_GENERIC_REPORT_SUCCESS    = 'API_POST_GENERIC_REPORT_SUCCESS'

export const reportsList = [{
    id: 1,
    name: 'Tom Usage Report 1',
    reportType: 'USAGE',
    reportStartDate: 1485453595000,
    reportEndDate: null
}, {
    id: 2,
    name: 'IMF Usage Report 2',
    reportType: 'USAGE',
    reportStartDate: 1485238420000,
    reportEndDate: null
}, {
    id: 3,
    name: 'IMF Usage Report 3',
    reportType: 'USAGE',
    reportStartDate: 1485238420000,
    reportEndDate: null
}, {
    id: 4,
    name: 'IMF Usage Report 4',
    reportType: 'USAGE',
    reportStartDate: 1485238420000,
    reportEndDate: null
}, {
    id: 5,
    name: 'IMF Usage Report 5',
    reportType: 'USAGE',
    reportStartDate: 1485238420000,
    reportEndDate: null
}, {
    id: 6,
    name: 'IMF Usage Report 6',
    reportType: 'USAGE',
    reportStartDate: 1485238420000,
    reportEndDate: null
}, {
    id: 7,
    name: 'IMF Usage Report 7',
    reportType: 'USAGE',
    reportStartDate: 1485238420000,
    reportEndDate: null
}, {
    id: 8,
    name: 'IMF Usage Report 8',
    reportType: 'USAGE',
    reportStartDate: 1485238420000,
    reportEndDate: null
}, {
    id: 9,
    name: 'IMF Usage Report 9',
    reportType: 'USAGE',
    reportStartDate: 1485238420000,
    reportEndDate: null
}, {
    id: 10,
    name: 'IMF Usage Report 10',
    reportType: 'USAGE',
    reportStartDate: 1485238420000,
    reportEndDate: null
}, {
    id: 11,
    name: 'IMF Usage Report 11',
    reportType: 'USAGE',
    reportStartDate: 1485238420000,
    reportEndDate: null
}]



export const genericList = {
    reportHeader: [
        'productname',
        'counterreporttype',
        'issn',
        'onlineissn',
        'isbn',
        'total',
        'feb_2016',
        'mar_2016',
        'apr_2016',
        'may_2016',
        'jun_2016',
        'jul_2016',
        'aug_2016',
        'sep_2016',
        'oct_2016',
        'nov_2016',
        'dec_2016',
        'jan_2017',
        'feb_2017'
    ],
    reportBody: [[
        'IMF',
        'DATABASE',
        '',
        '',
        '',
        '173329',
        '19122',
        '19842',
        '26059',
        '16661',
        '15314',
        '22389',
        '13862',
        '15930',
        '18443',
        '0',
        '0',
        '0',
        '0'
    ]]
}

export const dataRecurringJobLog = [
  {
    id: '588872f0451164df8e87121c',
    description: 'nostrud ipsum adipisicing consequat',
    user: 'laboris',
    startTime: '2016-07-16T12:54:59 -03:00',
    endTime: '2016-12-16T02:23:25 -02:00',
    runTime: '2015-08-07T06:49:34 -03:00'
  },
  {
    id: '588872f0a5762aa03deac77a',
    description: 'dolore ullamco in magna',
    user: 'sunt',
    startTime: '2016-01-01T03:21:55 -02:00',
    endTime: '2015-11-24T11:41:48 -02:00',
    runTime: '2015-09-25T12:07:15 -03:00'
  },
  {
    id: '588872f0c88a057d9e22959b',
    description: 'et excepteur consequat et',
    user: 'cupidatat',
    startTime: '2015-10-08T07:16:27 -03:00',
    endTime: '2015-02-22T03:58:57 -02:00',
    runTime: '2014-11-03T11:57:15 -02:00'
  },
  {
    id: '588872f09f62774043f3c5e3',
    description: 'id culpa mollit cupidatat',
    user: 'cupidatat',
    startTime: '2014-06-03T06:41:09 -03:00',
    endTime: '2016-08-07T07:06:20 -03:00',
    runTime: '2014-12-15T09:06:27 -02:00'
  },
  {
    id: '588872f0de62ef568430ad65',
    description: 'ut eu nulla incididunt',
    user: 'voluptate',
    startTime: '2014-06-21T01:06:20 -03:00',
    endTime: '2014-12-29T04:21:40 -02:00',
    runTime: '2015-08-30T09:15:27 -03:00'
  }
]
