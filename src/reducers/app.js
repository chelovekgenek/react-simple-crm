import * as c from '../constants'

const initialState = {
  data: [],
  settings: {},
  generic: {},
  fetching: false,
  error: false
}

export default function app(state = initialState, action) {
  switch (action.type) {
    case c.API_GET_ALL_REQUEST:
      return {...state, fetching: true}
    case c.API_GET_ALL_SUCCESS:
      return {...state, data: action.payload, fetching: false}
    case c.API_GET_ALL_ERROR:
      return {...state, fetching: false, error: action.payload}
    case c.API_GET_REPORT_SETTINGS_REQUEST:
      return {...state, fetching: true}
    case c.API_GET_REPORT_SETTINGS_SUCCESS:
      return {...state, settings: action.payload, fetching: false}
    case c.API_POST_GENERIC_REPORT_REQUEST:
      return {...state, fetching: true}
    case c.API_POST_GENERIC_REPORT_SUCCESS:
      return {...state, generic: action.payload, fetching: false}
    default:
      return state
  }
}
