import { push } from 'react-router-redux'
import * as c from '../constants'

const headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
}

export const getAll = () => dispatch => {
  dispatch({
    type: c.API_GET_ALL_REQUEST
  })
  const options = {
    method: 'POST',
    headers
  }
  fetch(c.API_GET_ALL_URL, options)
    .then(response => response.json())
    .then(result => {
      console.log(result)
      dispatch({type: c.API_GET_ALL_SUCCESS, payload: result.reportConfigResponse})
    })
    .catch(error => dispatch({type: c.API_GET_ALL_ERROR, payload: error}))
}

export const initContentUsageReport = () => dispatch => {
  dispatch({type: c.API_GET_REPORT_SETTINGS_REQUEST})
  const options = {
    method: 'POST',
    headers
  }
  fetch(c.API_GET_REPORT_SETTINGS_URL, options)
    .then(response => response.json())
    .then(result => {
      dispatch({
        type: c.API_GET_REPORT_SETTINGS_SUCCESS,
        payload: result
      })
    }
  )
}
export const saveContentUsageReport = (data) => dispatch => {
  dispatch({type: c.API_POST_REPORT_SETTINGS_REQUEST})
  const options = {
    method: 'POST',
    body: JSON.stringify(data),
    headers
  }
  fetch(c.API_POST_REPORT_SETTINGS_URL, options).then(
    res => {
      if (res.status == 200)
        dispatch({type: c.API_POST_REPORT_SETTINGS_SUCCESS})
      dispatch(push(c.ROUTE_LIST))
    }
  )
}
export const getGenericUsageReport = (id) => dispatch => {
  dispatch({type: c.API_POST_GENERIC_REPORT_REQUEST})
  const options = {
    method: 'POST',
    body: JSON.stringify({reportConfigId: id}),
    headers
  }
  fetch(c.API_POST_GENERIC_REPORT_URL, options)
  .then(res => res.json())
  .then(
    result => {
      dispatch({
        type: c.API_POST_GENERIC_REPORT_SUCCESS,
        payload: result
      })
      dispatch(push(c.ROUTE_GENERIC))
    }
  )
}

export const redirect = (nextUrl) => dispatch => {
  dispatch(push(nextUrl))
}
